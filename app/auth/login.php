<?php 
session_start();
require_once("../../resources/config.php");
require_once(FUNCTIONS_PATH."/conn.php");
if (isset($_SESSION["login"])) {
	header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["web"]."/index.php");
}

if(isset($_POST['login'])){

	$username = $_POST['username']; 
	$password = $_POST['password']; 
	$q = "SELECT * FROM users WHERE username = '$username'";
	$result = mysqli_query($conn,$q);
	if(mysqli_num_rows($result) === 1){
		$row = mysqli_fetch_assoc($result);
		if(password_verify($password, $row["password"])){
			header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["web"]."/index.php");
			$_SESSION["login"] = true;
			$_SESSION["id"] = $row["id"];
			$_SESSION["role"] = $row["role"];
			$_SESSION["name"] = $row["username"];
			exit;	
		}
	} 
	
	$error = true;

	
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=$config['urls']['baseUrl'].$config['paths']['resources']?>/css/bootstrap.css">
    <title>Login</title>
</head>
<body>
    <div class="container col-md-4 mt-4">
        <h1 class="text-center">Login</h1>

        <form action="" method="POST">
        <div class="form-group">
            <label for="username">username</label>
            <input type="text" class="form-control" id="username" name="username" required>
        </div>
        <div class="form-group">
            <label for="password">password</label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <button type="submit" class="btn btn-primary" name="login">login</button>
        <a href="register.php" class="btn btn-info">register</a>

        </form>

        <?php if(isset($error)): ?>
            <div class="alert alert-danger">
            username / password salah
            </div>
        <?php endif; ?>
    </div>
</body>
</html>