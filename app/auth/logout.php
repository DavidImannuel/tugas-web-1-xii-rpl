<?php 
require_once $_SERVER["DOCUMENT_ROOT"]."/tugas-web/resources/config.php";

session_start();

session_destroy();

$url = $config["urls"]['baseUrl'].$config["urls"]['auth']."/login.php";

header("Location: $url");
exit;

 ?>