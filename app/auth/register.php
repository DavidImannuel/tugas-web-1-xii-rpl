<?php 
session_start();
require_once("../../resources/config.php");
require_once(FUNCTIONS_PATH."/conn.php");

if (isset($_POST['register'])) {
	$username = $_POST["username"];
	$password = $_POST["password"];
	$password_hash = password_hash($password, PASSWORD_DEFAULT);

	$register = mysqli_query($conn,"INSERT INTO users VALUES(NULL,'$username','$password_hash',0)");
	if($register){

        $url = $config["urls"]['baseUrl'].$config["urls"]['auth']."/login.php";
		header("Location: $url");
		exit;
	} 
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=$config['urls']['baseUrl'].$config['paths']['resources']?>/css/bootstrap.css">
    <title>Login</title>
</head>
<body>
    <div class="container col-md-4 mt-4">
        <div class="container">
            <h1 class="text-center">Register</h1>
            <form action="" method="POST">
            <div class="form-group">
                <label for="username">username</label>
                <input type="text" class="form-control" id="username" name="username" required>
            </div>
            <div class="form-group">
                <label for="password">password</label>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>
            <button type="submit" class="btn btn-primary" name="register">register</button>
            </form>
        </div>
    </div>
</body>
</html>