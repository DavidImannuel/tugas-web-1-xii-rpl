
<?php    
    session_start();
    // load up your config file
    require_once("../../../resources/config.php");
     
    require_once(FUNCTIONS_PATH . "/query.php");
    require_once(TEMPLATES_PATH . "/header.php");
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }

    if(isset($_POST['tambah'])){
        $id_user = $_POST["user"];
        $nama = $_POST["nama"];
        $id_mapel = $_POST["mapel"];

        //ubah role user menjadi 2 == guru
        mysqli_query($conn,"UPDATE users SET role = 2 WHERE id = $id_user");

        $tambah = mysqli_query($conn,"INSERT INTO guru VALUES(NULL,'$id_user','$nama','$id_mapel')");

        if($tambah){
            echo "<div class='alert alert-success' role='alert'>
            Data Berhasil Ditambah
          </div>";
        } else {
            echo "error" . mysqli_error($conn);
        }
    }

    $mapel = fetch_data("mapel");
    $users = custom_fetch_data("SELECT * FROM users WHERE role = 0");
    $data = custom_fetch_data("SELECT guru.*, mapel.nama as nama_mapel FROM guru INNER JOIN mapel ON guru.id_mapel = mapel.id");

    if(isset($_GET["id"])){
        $editguru = fetch_single_data("guru",$_GET["id"]) ;
        // var_dump($editguru);
    }
    if(isset($_POST["edit"])){
        $id = $_POST["id"];
        $nama = $_POST["nama"];
        $id_mapel = $_POST["mapel"];

        $edit = mysqli_query($conn,"UPDATE guru SET nama = '$nama',id_mapel = '$id_mapel' WHERE id = $id");

        if($edit){
            header("Refresh: 0");
        }
    }
    
?>

<div class="container">
<div class="row">
    <div class="col-md-6">
        <h4>Guru</h4>
        <?php if( isset($_GET["id"]) ) :?>
            <form action="" method="POST">
            <input type="hidden" name="id" value="<?=$editguru["id"]?>">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input autocomplete="off" required type="text" class="form-control" id="nama" name="nama" value="<?=$editguru["nama"]?>">
            </div>
            <div class="form-group">
                <label for="mapel">Mapel</label>
                <select name="mapel" class="form-control" >
                    <option disabled selected>Pilih mapel..</option>
                    <?php foreach($mapel as $row):?>
                        <option 
                        <?=$editguru["id_mapel"] === $row["id"] ? "selected" : "" ?>
                        value="<?=$row["id"]?>" ><?=$row["nama"]?></option>                
                    <?php endforeach;?>
                </select>
            </div>
            <button type="submit" name="edit" class="btn btn-primary">Edit</button>
            </form>	
        <?php else :?>
            <form action="" method="POST">
            <div class="form-group">
                <label for="user">User yang dijadikan guru</label>
                <select name="user" class="form-control" >
                    <option disabled selected>Pilih Users..</option>
                    <?php foreach($users as $row):?>
                        <option value="<?=$row["id"]?>" ><?=$row["username"]?></option>                
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label for="nama">Nama Guru</label>
                <input type="text" name="nama" id="nama" class="form-control">
            </div>
            <div class="form-group">
                <label for="mapel">Mapel</label>
                <select name="mapel" class="form-control" >
                    <option disabled selected>Pilih mapel..</option>
                    <?php foreach($mapel as $row):?>
                        <option value="<?=$row["id"]?>" ><?=$row["nama"]?></option>                
                    <?php endforeach;?>
                </select>
            </div>
            <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
            </form>	
        <?php endif; ?>
    </div>
    <div class="col-md-6 mt-4">
        <table class="table table-reponsive">
            <thead>
                <tr>
                <!-- <th scope="col">#</th> -->
                <th scope="col">Guru</th>
                <th scope="col">Mapel</th>
                <th scope="col">Opsi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $id=>$guru):?>
                    <tr>
                        <td><a href="index.php?id=<?=$guru['id'];?>"><?=$guru['nama']?></a></td>
                        <td><?=$guru['nama_mapel']?></td>
                        <td>
                            <a onclick="return confirm('apakah yakin akan menghapus')" href="hapus.php?id=<?=$guru['id'];?>" id='hapus' class="btn btn-danger">hapus</a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

</div>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>