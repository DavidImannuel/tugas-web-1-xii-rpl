
<?php    
    session_start();
    // load up your config file
    require_once("../../../resources/config.php");
     
    require_once(FUNCTIONS_PATH . "/query.php");
    require_once(TEMPLATES_PATH . "/header.php");
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }
    //pagination
    $halaman = 4;
	$page = isset($_GET['halaman'])?(int)$_GET['halaman']:1;
	$mulai = ($page > 1)? ($page * $halaman) - $halaman:0;

    $mapel = fetch_data("mapel");
    $siswa = custom_fetch_data("SELECT siswa.*, jurusan.nama AS nama_jurusan FROM siswa INNER JOIN jurusan ON siswa.id_jurusan = jurusan.id");
    $id_user = fetch_single_data("users",$_SESSION["id"]);
    $sql = mysqli_query($conn,"SELECT * FROM guru WHERE id_user = {$id_user['id']}");
    $guru = mysqli_fetch_assoc($sql);
    $id_guru = $guru['id'];
    $mapel_guru = $guru['id_mapel'];


    // dd($_SESSION["role"]);
    if($_SESSION["role"]==1){
        $id_siswa = mysqli_query($conn,"SELECT * FROM siswa WHERE id_user = {$_SESSION["id"]}");

        $getIdSiswa = mysqli_fetch_assoc($id_siswa);
        // dd($getIdSiswa);

        if(isset($_GET['q'])){
            $q = mysqli_real_escape_string($conn,$_GET['q']);
            $dataCari = $_GET['q'];

            $data = custom_fetch_data("SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id WHERE id_siswa = {$getIdSiswa['id']} AND  
            mapel.nama LIKE '%$q%' OR id_siswa = {$getIdSiswa['id']} AND  
            guru.nama LIKE '%$q%' 
            LIMIT $mulai, $halaman");

            $data_penuh = mysqli_query($conn,"SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id WHERE id_siswa = {$getIdSiswa['id']} AND 
            mapel.nama LIKE '%$q%' OR id_siswa = {$getIdSiswa['id']} AND  
            guru.nama LIKE '%$q%' 
            LIMIT $mulai, $halaman");

        } else {
            $data = custom_fetch_data("SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id WHERE id_siswa = {$getIdSiswa['id']} LIMIT $mulai, $halaman");

            $data_penuh = mysqli_query($conn,"SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id WHERE id_siswa = {$getIdSiswa['id']}");
        }

    } else {

        if(isset($_GET['q'])){
            $q = mysqli_real_escape_string($conn,$_GET['q']);
            $dataCari = $_GET['q'];
            $data = mysqli_query($conn,"SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id WHERE 
            siswa.nama LIKE '%$q%' OR 
            mapel.nama LIKE '%$q%' 
            LIMIT $mulai, $halaman");

            $data_penuh = mysqli_query($conn,"SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id WHERE 
            siswa.nama LIKE '%$q%' OR 
            mapel.nama LIKE '%$q%' ");

        } else {
            $data = mysqli_query($conn,"SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id LIMIT $mulai, $halaman");

            $data_penuh = mysqli_query($conn,"SELECT nilai.* , guru.nama namaguru , siswa.nama namasiswa ,mapel.nama namamapel FROM nilai INNER JOIN guru ON nilai.id_guru = guru.id INNER JOIN siswa ON nilai.id_siswa = siswa.id INNER JOIN mapel ON nilai.id_mapel = mapel.id");
        }

    }

    //pagination
    $total = mysqli_num_rows($data_penuh);	
	$pages = ceil($total / $halaman);

    // dd($data);


    if(isset($_POST['tambah'])){

        $id_mapel = $_POST["id_mapel"];
        $id_siswa = $_POST["siswa"];
        $uts = $_POST["uts"];
        $uas = $_POST["uas"];
        $tunggal = $_POST["tunggal"];

        $tambah = mysqli_query($conn,"INSERT INTO nilai VALUES(NULL,$id_guru,$id_siswa,$id_mapel,$uts,$uas,$tunggal)");

        if($tambah){
            echo "<div class='alert alert-success' role='alert'>
            Data Berhasil Ditambah
          </div>";

          header("Refresh: 0");
        } else {
            echo "error" . mysqli_error($conn);
        }
    }
    
?>

<div class="container">
<div class="row">
    <?php if($_SESSION["role"] == 2) :?>
        <div class="col-md-6">
            <h4>Tambah Nilai Siswa</h4>
                <form action="" method="POST">
                <div class="form-group">
                    <label for="mapel">Mapel</label>
                    <select name="mapel" id="mapel" class="form-control" disabled>
                    <option selected disabeld >Pilih mapel..</option>
                    <?php foreach($mapel as $row):?>
                        <option value="<?=$row["id"]?>" <?=($row["id"]==$mapel_guru) ? "selected" : "" ?> ><?=$row["nama"]?></option>
                    <?php endforeach;?>
                    </select>
                    <input type="hidden" value="" name="id_mapel" id="id_mapel">
                </div>
                <div class="form-group">
                    <label for="siswa">Siswa</label>
                    <select name="siswa" id="siswa" class="form-control">
                    <option selected disabeld >Pilih siswa..</option>
                    <?php foreach($siswa as $row):?>
                        <option value="<?=$row["id"]?>"><?=$row["nama"]. '-' .$row["kelas"]. '-' .$row["nama_jurusan"]?></option>
                    <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="uts" id="uts"> Nilai UTS</label>
                    <input type="number" name="uts" id="uts" class="form-control" max="100" >
                </div>
                <div class="form-group">
                    <label for="uas" id="uas"> Nilai UAS</label>
                    <input type="number" name="uas" id="uas" class="form-control" max="100" >
                </div>
                <div class="form-group">
                    <label for="tunggal" id="tunggal"> Nilai Tunggal</label>
                    <input type="number" name="tunggal" id="tunggal" class="form-control" max="100" >
                </div>
                <button type="submit" name="tambah" class="btn btn-primary" onclick='return confirm("anda yakin untuk menyimpan, karena data yang tersimpan tidak dapat diubah")'>Tambah</button>
                </form>
        </div>
    <?php endif;?>
    <div class="col-<?= $_SESSION["role"] == 2 ? "md-6" : "md-12" ?> mt-4">

    <?php if($_SESSION["role"] > 0):?>
    <h4>Daftar Nilai Siswa</h4>
        <table class="table table-reponsive">
            <thead>
                <tr>
                <th scope="col">No</th>
                <th scope="col">Siswa</th>
                <th scope="col">Guru</th>
                <th scope="col">Mapel</th>
                <th scope="col">UTS</th>
                <th scope="col">UAS</th>
                <th scope="col">Nilai Tunggal</th>
                <!-- <th scope="col">Opsi</th> -->
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $guru):?>
                    <tr>
                        <td><?=++$mulai?></td>
                        <td><?=$guru['namasiswa']?></td>
                        <td><?=$guru['namaguru']?></td>
                        <td><?=$guru['namamapel']?></td>
                        <td><?=$guru['nilai_uts']?></td>
                        <td><?=$guru['nilai_uas']?></td>
                        <td><?=$guru['nilai_tunggal']?></td>
                        <!-- <td>
                            <a onclick="return confirm('apakah yakin akan menghapus')" href="hapus.php?id=<?=$guru['id'];?>" id='hapus' class="btn btn-danger">hapus</a>
                        </td> -->
                    </tr>
                <?php endforeach;?>
            </tbody>
            <?php else:?>
            <h1>Harap lapor admin untuk mengaktifkan akun anda sebagai siswa / guru</h1>
            <?php endif;?>
        </table>
        <nav>
            <ul class="pagination justify-content-center">
                <?php for ($i=1; $i <= $pages ; $i++): ?>
                    <?php if(isset($_GET['q'])) :?>
                        <li class="page-item"><a class="page-link" href="?q=<?= $dataCari; ?>&halaman=<?= $i; ?>"><?= $i; ?></a></li>
                    <?php else: ?>
                        <li class="page-item <?php if(isset($_GET["halaman"])){echo ($i==$_GET["halaman"]?"active":"");} ?>">
                            <a class="page-link " href="?halaman=<?= $i; ?>">
                                <?= $i; ?> 
                                <?php if (isset($_GET["halaman"])) {
                                    echo ($_GET["halaman"]==$i?"<span class='sr-only'>(current)</span>":"");
                                } ?>
                            </a> 
                        </li>
                    <?php endif; ?>
                    
                <?php endfor; ?>
            </ul>
        </nav>
    </div>
</div>

</div>
<script>
    var mapel = document.querySelector("#mapel");
    var id_mapel = document.querySelector("#id_mapel");

    id_mapel.value = mapel.value;  
    

</script>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>