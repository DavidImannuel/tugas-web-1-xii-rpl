
<?php    
    session_start();
    
    // load up your config file
    require_once("path/to/resources/config.php");
     
    require_once(TEMPLATES_PATH . "/header.php");
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }
?>

<div class="container">

<!-- your content here -->

</div>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>