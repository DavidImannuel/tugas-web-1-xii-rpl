
<?php    
    // load up your config file
    require_once("../../../resources/config.php");
     
    session_start();
    require_once(FUNCTIONS_PATH . "/query.php");
    require_once(TEMPLATES_PATH . "/header.php");
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }

    if(isset($_POST['tambah'])){
        $nama = $_POST["nama"];
        $jumlah_jam = $_POST["jumlah_jam"];

        $tambah = mysqli_query($conn,"INSERT INTO mapel VALUES(NULL,'$nama','$jumlah_jam')");

        if($tambah){
            echo "<div class='alert alert-success' role='alert'>
            Data Berhasil Ditambah
          </div>";
        }
    }

    $data = fetch_data("mapel");
    // var_dump($data);

?>

<div class="container">
    <h1>Mata Pelajaran</h1>
    <form action="" method="POST">
	  <div class="form-group">
	    <label for="nama">Nama</label>
	    <input autocomplete="off" required type="text" class="form-control" id="nama" name="nama" >
	  </div>
	  <div class="form-group">
	    <label for="nama">Jumlah Jam</label>
	    <input autocomplete="off" required type="number" class="form-control" id="nama" name="jumlah_jam" >
	  </div>
	  <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
	</form>	


    <table class="table mt-4">
        <thead>
            <tr>
            <!-- <th scope="col">#</th> -->
            <th scope="col">Nama</th>
            <th scope="col">Jumlah Jam</th>
            <th scope="col">Mapel_ID</th>
            <th scope="col">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data as $id=>$mapel):?>
                <tr>
                    <td><?=$mapel['nama']?></td>
                    <td><?=$mapel['jumlah_jam']?></td>
                    <td><?=$mapel['id']?></td>
                    <td>
                        <a onclick="return confirm('apakah yakin akan menghapus')" href="hapus.php?id=<?=$mapel['id'];?>" id='hapus' class="btn btn-danger">hapus</a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>

</div>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>