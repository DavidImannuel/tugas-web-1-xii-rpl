
<?php    
    session_start();
    // load up your config file
    require_once("../../resources/config.php");
     
    require_once(TEMPLATES_PATH . "/header.php");
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }
?>

<div class="container">

<div class="jumbotron mt-4">
  <h1 class="display-4">Hello, <?=$_SESSION["name"];?></h1>
  <p class="lead">School-App adalah aplikasi berbasis web yang memudahkan guru megisi nilai pada siswa secara online</p>
  <hr class="my-4">
  <p>siswa pn dapat melihat nilai yang diberikan oleh gurunya</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/nilai"?>" role="button">Lihat Nilai</a>
  </p>
</div>

</div>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>