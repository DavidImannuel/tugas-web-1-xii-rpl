
<?php    
    session_start();
    // load up your config file
    require_once("../../../resources/config.php");
     
    require_once(FUNCTIONS_PATH . "/query.php");
    require_once(TEMPLATES_PATH . "/header.php");
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }

    $users = fetch_data('users');
    // dd($users);
?>

<div class="container">
<h3>Users</h3>
    <table class="table mt-4">
        <thead>
            <tr>
            <!-- <th scope="col">#</th> -->
            <th scope="col">Nama</th>
            <th scope="col">Role</th>
            <th scope="col">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($users as $user):?>
                <tr>
                    <td><?=$user['username']?></td>
                    <td><?php 
                        if($user["role"] == 3) {
                            echo "admin";
                        } else if($user["role"] ==  2){
                            echo "guru";
                        } else if($user["role"] ==  1){
                            echo "siswa";
                        } else {
                            echo "user";
                        }
                    ?></td>
                    <td>
                        <a onclick="return confirm('apakah yakin akan menghapus')" href="hapus.php?id=<?=$user['id'];?>" id='hapus' class="btn btn-danger">hapus</a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>

</div>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>