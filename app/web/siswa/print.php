<?php    
    // load up your config file
    require_once("../../../resources/config.php");
     
    require_once(TEMPLATES_PATH . "/header.php");
    require_once(FUNCTIONS_PATH . "/query.php");
    session_start();
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }

    $siswa = custom_fetch_data("SELECT siswa.*, jurusan.nama AS nama_jurusan FROM siswa INNER JOIN jurusan ON siswa.id_jurusan = jurusan.id");
    // var_dump($data_penuh);die;
?>

<div class="container">

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Jurusan</th>
      <th scope="col">NIS</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($siswa as $key=>$row):?>
        <tr>
        <td><?=$key+1?></td>
        <td><?=$row["nama"]?></td>
        <td><?=$row["nama_jurusan"]?></td>
        <td><?=$row["nis"]?></td>
        </tr>
    <?php endforeach;?>
  </tbody>
</table>

</div>
<script>
    window.print();
</script>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>