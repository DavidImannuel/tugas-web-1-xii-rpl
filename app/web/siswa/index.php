
<?php    
    // load up your config file
    require_once("../../../resources/config.php");
     
    session_start();
    require_once(FUNCTIONS_PATH . "/query.php");
    require_once(TEMPLATES_PATH . "/header.php");
    if (!$_SESSION["login"]) {
      header("Location: ".$config["urls"]["baseUrl"].$config["urls"]["auth"]."/login.php");
    }
?>

<div class="container">

<!-- your content here -->

<?php 
	//search dan pagination
	$halaman = 4;
	$page = isset($_GET['halaman'])?(int)$_GET['halaman']:1;
	$mulai = ($page > 1)? ($page * $halaman) - $halaman:0;
	// var_dump($mulai);die;
	if(isset($_GET['q'])) {
		$q = mysqli_real_escape_string($conn,$_GET['q']);
		$dataCari = $_GET['q'];
		$data = mysqli_query($conn,"SELECT siswa.*, jurusan.nama AS nama_jurusan FROM siswa INNER JOIN jurusan ON siswa.id_jurusan = jurusan.id WHERE 
					siswa.nama LIKE '%$q%' OR 
					jurusan.nama LIKE '%$q%' OR 
					siswa.nis LIKE '%$q%' LIMIT $mulai, $halaman");
		$data_penuh = mysqli_query($conn,"SELECT siswa.*, jurusan.nama AS nama_jurusan FROM siswa INNER JOIN jurusan ON siswa.id_jurusan = jurusan.id WHERE 
					siswa.nama LIKE '%$q%' OR 
					jurusan.nama LIKE '%$q%' OR 
					siswa.nis LIKE '%$q%'");

	} else {		
		$data = mysqli_query($conn,"SELECT siswa.*, jurusan.nama AS nama_jurusan FROM siswa INNER JOIN jurusan ON siswa.id_jurusan = jurusan.id LIMIT $mulai, $halaman ");	
		$data_penuh = mysqli_query($conn, "SELECT * FROM siswa" );
		// $data_penuh = custom_fetch_data("SELECT siswa.*, jurusan.nama AS nama_jurusan FROM siswa INNER JOIN jurusan ON siswa.id_jurusan = jurusan.id");
		// var_dump($data_penuh);die;


	}
	$total = mysqli_num_rows($data_penuh);	
	$pages = ceil($total / $halaman);

	 ?>
	 <a href="print.php" target="__BLANK" class="btn btn-warning">Print</a>
	 <a href="form.php" class="btn btn-success">Tambah Siswa</a>
	<table class="table table-striped mt-3 table-responsive-lg table-hover">
	  <thead class="thead-light">
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">Nama</th>
	      <th scope="col">Kelas</th>
	      <th scope="col">Jurusan</th>
	      <th scope="col">NIS</th>
	      <?php if($_SESSION["role"] == 3) : ?>
	      <th scope="col">Opsi</th>
		  <?php endif; ?>  
	      
	    </tr>
	  </thead>
	  <tbody id="list">
	  	<?php while ($rows = mysqli_fetch_assoc($data)) :?>
		    <tr>
		      <th scope="row"><?=++$mulai;?></th>
		      <td><?=$rows["nama"];?></td>
		      <td><?=$rows["kelas"];?></td>
		      <td><?=$rows["nama_jurusan"];?></td>
		      <td><?=$rows["nis"];?></td>
		      <?php if($_SESSION["role"] == 3) : ?>
		      <td>
		      	<a onclick="return confirm('apakah yakin akan menghapus')" href="hapus.php?id=<?=$rows['id'];?>" id='hapus' class="btn btn-danger">hapus</a>
		      	<a href="form.php?id=<?=$rows['id'];?>" class="btn btn-primary">edit</a>
			  </td>
			</tr>
			  <?php endif; ?>  
		<?php endwhile;?>
	  </tbody>
	</table>
	<nav>
	  <ul class="pagination justify-content-center">
	    <?php for ($i=1; $i <= $pages ; $i++): ?>
	    	<?php if(isset($_GET['q'])) :?>
			    <li class="page-item"><a class="page-link" href="?q=<?= $dataCari; ?>&halaman=<?= $i; ?>"><?= $i; ?></a></li>
	    	<?php else: ?>
			    <li class="page-item <?php if(isset($_GET["halaman"])){echo ($i==$_GET["halaman"]?"active":"");} ?>">
			    	<a class="page-link " href="?halaman=<?= $i; ?>">
				    	<?= $i; ?> 
				    	<?php if (isset($_GET["halaman"])) {
				    		echo ($_GET["halaman"]==$i?"<span class='sr-only'>(current)</span>":"");
				    	} ?>
			    	</a> 
			    </li>
	    	<?php endif; ?>
			
		<?php endfor; ?>
	  </ul>
	</nav>

</div>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>