
<?php    
    session_start();
    // load up your config file
    require_once("../../../resources/config.php");
     
    require_once(FUNCTIONS_PATH . "/query.php");
	require_once(TEMPLATES_PATH . "/header.php");
	$jurusan = fetch_data("jurusan");
    $users = custom_fetch_data("SELECT * FROM users WHERE role = 0");

    if($_SESSION["role"] != 3){
        echo "<script>
            alert('ANDA BUKAN ADMIN !!!');
            window.location.href = 'index.php';
        </script>";
    }
?>

<?php if(isset($_GET['id'])) : ?>
	
	<?php 
	$id = $_GET['id'];
	$data = mysqli_query($conn,"SELECT * FROM siswa WHERE id = '$id'");
	$row = mysqli_fetch_assoc($data);
	// var_dump($row);
	 ?>

	<div class="container">
		<h1>Edit </h1>
		<form action="" method="POST">
		  <div class="form-group">
		    <label for="nama">Nama</label>
		    <input autocomplete="off" required type="text" class="form-control" id="nama" name="nama" value="<?=$row['nama'];?>" >
		  </div>
		  <div class="form-group">
		    <label for="kelas">Kelas</label>
		    <select name="kelas" class="form-control">
		    	<option disabled>Pilih kelas..</option>
		    	<option value="X" <?= $row['kelas'] === 'X' ? "selected" : "";?>>X</option>
		    	<option value="XI" <?= $row['kelas'] === 'XI' ? "selected": "" ;?>>XI</option>
		    	<option value="XII" <?= $row['kelas'] === 'XII' ? "selected": "" ;?>>XII</option>
		    </select>
		  </div>
		  <div class="form-group">
			<label for="jurusan">Jurusan</label>
			<select name="jurusan" class="form-control" >
				<option disabled selected>Pilih jurusan..</option>
				<?php foreach($jurusan as $j):?>
					<option value="<?=$j["id"]?>" 
					<?=$row["id_jurusan"] === $j["id"] ? "selected" : "" ?>
					><?=$j["nama"]?></option>                
				<?php endforeach;?>
			</select>
		</div>
		  <div class="form-group">
		    <label for="nis">NIS</label>
		    <input autocomplete="off" required type="text" class="form-control" id="nis" name="nis" value="<?=$row['nis'];?>" >
		  </div>
		  <button type="submit" name="edit" class="btn btn-primary">Edit</button>
		</form>	
	</div>
	<?php  
		if (isset($_POST['edit'])) {
			$nama = $_POST['nama'];
			$kelas = $_POST['kelas'];
			$id_jurusan = $_POST['jurusan'];
			$nis = $_POST['nis'];
			$sql = "UPDATE siswa SET nama = '$nama' , kelas = '$kelas',id_jurusan = '$id_jurusan',nis = '$nis' WHERE id = '$id'";

			if (mysqli_query($conn,$sql)) {
				header("Location: index.php");
			} else {
				echo "eror : " . mysqli_error($conn);
			}
		}
	?>

<?php else : ?>
<?php  ?>
<div class="container">
	<form action="" method="POST">
		<div class="form-group">
			<label for="user">User yang dijadikan Murid</label>
			<select name="user" class="form-control" >
				<option disabled selected>Pilih Users..</option>
				<?php foreach($users as $row):?>
					<option value="<?=$row["id"]?>" ><?=$row["username"]?></option>                
				<?php endforeach;?>
			</select>
		</div>
	  <div class="form-group">
	    <label for="nama">Nama</label>
	    <input autocomplete="off" required type="text" class="form-control" id="nama" name="nama" >
	  </div>
	  <div class="form-group">
	    <label for="kelas">Kelas</label>
	    <select name="kelas" class="form-control">
	    	<option disabled selected>Pilih kelas..</option>
	    	<option value="X">X</option>
	    	<option value="XI">XI</option>
	    	<option value="XII">XII</option>
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="jurusan">Jurusan</label>
	    <select name="jurusan" class="form-control" >
			<option disabled selected>Pilih jurusan..</option>
			<?php foreach($jurusan as $row):?>
				<option value="<?=$row["id"]?>" ><?=$row["nama"]?></option>                
            <?php endforeach;?>
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="nis">NIS</label>
	    <input autocomplete="off" required type="text" class="form-control" id="nis" name="nis" >
	  </div>
	  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
	</form>	
</div>

<?php 


if(isset($_POST['simpan'])){
	
	$nama = $_POST['nama'];
	$id_user = $_POST["user"];
	$kelas = $_POST['kelas'];
	$id_jurusan = $_POST['jurusan'];
	$nis = $_POST['nis'];
	$sql = "INSERT INTO siswa VALUES(NULL,'$nama','$id_user','$kelas','$id_jurusan','$nis')";
	// dd($sql);


	if (mysqli_query($conn,$sql)) {
	
		//ubah role menajadi 1 == murid;

		mysqli_query($conn,"UPDATE users SET role = 1 WHERE id = $id_user");
	
		echo "<script>
			alert('Data Berhasil Ditambah');
			window.location.href='index.php';
		</script>";
	} else {
		echo "error : " . mysqli_error($conn);
	}
} 
?>
<?php endif; ?>
<?php
    require_once(TEMPLATES_PATH . "/footer.php");
?>