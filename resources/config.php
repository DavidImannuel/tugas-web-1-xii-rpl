<?php

$project_name = "School-App";
 
$config = array(
    "db" => array(
            "dbname" => "web-1",
            "username" => "root",
            "password" => "root",
            "host" => "localhost"
    ),
    "urls" => array(
        "baseUrl" => "http://localhost/tugas-web",
        "web" => "/app/web",
        "auth"  => "/app/auth" 
    ),
    "paths" => array(
        "resources" => "/resources",
        "img" => "/img",

    ),
);
 
defined("FUNCTIONS_PATH")
    or define("FUNCTIONS_PATH", realpath(dirname(__FILE__) . '/functions'));
     
defined("TEMPLATES_PATH")
    or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));

 
/*
    Error reporting.
*/
ini_set("error_reporting", "true");
error_reporting(E_ALL|E_STRCT);
 
?>