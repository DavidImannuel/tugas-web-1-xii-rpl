-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 21, 2019 at 02:12 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web-1`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_mapel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `id_user`, `nama`, `id_mapel`) VALUES
(18, 21, 'Guru PBO', 6),
(19, 22, 'Guru Web', 7),
(20, 23, 'Guru KE 3', 5);

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id`, `nama`) VALUES
(1, 'Rekayasa Perangkat Lunak'),
(2, 'Teknik Komputer Jaringan'),
(3, 'Animasi'),
(4, 'Sistem Informasi dan Jaringan'),
(5, 'Teknik Kendaraan Ringan'),
(6, 'Audio Video'),
(8, 'Kecantikan'),
(9, 'Tata Boga');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jumlah_jam` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id`, `nama`, `jumlah_jam`) VALUES
(1, 'Matematika', 5),
(2, 'Fisika', 4),
(3, 'Basis Data', 8),
(5, 'Pemrogaman Dasar', 8),
(6, 'Pemrogaman Berorientasi Objek', 6),
(7, 'Pemrogaman Web', 12);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nilai_uts` int(11) NOT NULL,
  `nilai_uas` int(11) NOT NULL,
  `nilai_tunggal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id`, `id_guru`, `id_siswa`, `id_mapel`, `nilai_uts`, `nilai_uas`, `nilai_tunggal`) VALUES
(9, 18, 37, 6, 70, 80, 90),
(10, 19, 38, 7, 90, 90, 90),
(11, 19, 37, 7, 70, 80, 90),
(13, 18, 41, 6, 70, 70, 94),
(14, 19, 41, 7, 80, 80, 80),
(15, 18, 42, 6, 50, 90, 30),
(16, 19, 42, 7, 90, 90, 90),
(17, 20, 42, 5, 80, 80, 80),
(18, 18, 44, 6, 90, 90, 90);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `nis` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `nama`, `id_user`, `kelas`, `id_jurusan`, `nis`) VALUES
(37, 'Mohan', 15, 'XII', 8, '123321'),
(38, 'David', 16, 'XII', 1, '1233211223'),
(39, 'Adiv', 17, 'X', 4, '12332111'),
(41, 'test', 18, 'XI', 2, '213123312'),
(42, 'Ferdy Septiawan', 26, 'XII', 1, '47629803'),
(43, 'murid5', 19, 'XII', 2, '213123'),
(44, 'muridbaru', 27, 'XII', 1, '671863876');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(300) NOT NULL,
  `role` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(2, 'admin', '$2y$10$ak5INhqcMz9HGVBfaPiz7.6R9VWTzDZmSoIAUqgzP2aZZ.r/QNxJu', 3),
(15, 'murid1', '$2y$10$AhIBOvzXvmBX7.A5hHXM0OKYFSuC5ITWE3jOZmZz5m85GjAshaS.6', 1),
(16, 'murid2', '$2y$10$wcIFkvahtxa5vQjAv/Mst.NaZtjpmQJTlqmNtFrmGlCmUpoNpCjiS', 1),
(17, 'murid3', '$2y$10$Ly5KrQbCfXe2wbxJkKT7IuovJdIgjeLfo6yHQEy7nKdu9NvzuM.F2', 1),
(18, 'murid4', '$2y$10$w4w1YhtzA8KvGHjXbKFL6.qEEl4d9eRAosr07j94t3HB92tudoA8u', 1),
(19, 'murid5', '$2y$10$WTy1TnDNW5HISiwK3QrVqOMWH7/9jxDonpBZvlSV0yEPO3b0taUqG', 1),
(21, 'guru', '$2y$10$UJoyaq4t9QjfWuMQ1inhYeWk4suuu0d40DrNb9.Hh5mLNtMksfRdK', 2),
(22, 'guru2', '$2y$10$Jh5kEkCOolmxtlAtyoLM3OO/gWr9LAGqMhymFBWn3PWQz88xo3oQC', 2),
(23, 'guru3', '$2y$10$tDUr1p1zyctbAIc9rd3n..ijmM.r/XkcKkh3QEqursTChtJqBL.4i', 2),
(24, 'guru4', '$2y$10$B7FLZcEiNCnzd/QP2mGvnuR7SrwHc18X4P/ci//rLBeiv6/umYrQy', 0),
(25, 'guru5', '$2y$10$ZbjqwEdQTpbxuaqcC/D7aenPvj1J/p8FmV8uQqCIKdA2IB4lGVy0O', 0),
(26, 'ferdy', '$2y$10$j.jdOP1UNQ9E16my4TG7B.8bb/wt6abCwdt0xnS4xhgwDNQo4ODfC', 1),
(27, 'muridbaru', '$2y$10$60E1MIXQg7dR0MrUPwjt8O8DLnMQJsUH5xmgyg/v1vXXXJwloAFRO', 1),
(28, 'userbaru', '$2y$10$iKG4mJMQvSdnncFQNgRtx.Yp5Zfsdh5fpd2LcimcI4MYjThASQHG6', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_mapel` (`id_mapel`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nilai_ibfk_1` (`id_mapel`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa_ibfk_1` (`id_jurusan`),
  ADD KEY `siswa_ibfk_2` (`id_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `guru_ibfk_1` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guru_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai_ibfk_1` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_2` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_3` FOREIGN KEY (`id_siswa`) REFERENCES `siswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
