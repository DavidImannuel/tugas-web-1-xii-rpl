<nav class="navbar navbar-expand-lg navbar-light bg-light" >
  <a class="navbar-brand" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/index.php"?>"><?=$project_name?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <!-- <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li> -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          List Item
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php if( isset($_SESSION) && $_SESSION["role"] == 3):?>
          <a class="dropdown-item" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/user/index.php"?>">Users</a>
            <a class="dropdown-item" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/siswa/index.php"?>">Siswa</a>
            <a class="dropdown-item" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/guru/index.php"?>"">Guru</a>
            <a class="dropdown-item" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/jurusan/index.php"?>"">Jurusan</a>
            <a class="dropdown-item" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/mapel/index.php"?>"">Mapel</a>
          <?php endif;?>
          <a class="dropdown-item" href="<?=$config["urls"]["baseUrl"].$config["urls"]["web"]."/nilai/index.php"?>"">Nilai</a>
        </div>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li> -->
    </ul>
    <form class="form-inline my-2 my-lg-0" method="GET" action="">
      <input class="form-control mr-sm-2" name="q" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      <a href="<?=$config["urls"]['baseUrl'].$config["urls"]['auth']."/logout.php"?>" class="btn btn-outline-danger my-2 my-sm-0" >logout</a>
    </form>
  </div>
</nav>