<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/tugas-web/resources/config.php";

$conn = mysqli_connect(
    $config["db"]["host"],
    $config["db"]["username"],
    $config["db"]["password"],
    $config["db"]["dbname"]
);

function fetch_data($table){
        global $conn;
        $data = [];
        $query = mysqli_query($conn,"SELECT * FROM $table");

        while ($rows = mysqli_fetch_assoc($query)) {
            $data[] = $rows;
        }

        return $data;
        
    }

function fetch_single_data($table,$id){
        global $conn;
        $data;
        $query = mysqli_query($conn,"SELECT * FROM $table WHERE id = '$id'");

        while ($rows = mysqli_fetch_assoc($query)) {
            $data = $rows;
        }

        return $data;
        
    }

function custom_fetch_data($sql){
        global $conn;
        $data = [];
        $query = mysqli_query($conn,$sql);

        if($query){

            while ($rows = mysqli_fetch_assoc($query)) {
                $data[] = $rows;
            }
        } else {
            $data['error'] = "error";
        }

        return $data;
        
    }

function dd($var){
    var_dump($var);
    die;
}
