<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/tugas-web/resources/config.php";

$conn = mysqli_connect(
    $config["db"]["host"],
    $config["db"]["username"],
    $config["db"]["password"],
    $config["db"]["dbname"]
);